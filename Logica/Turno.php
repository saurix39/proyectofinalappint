<?php
require_once "Persistencia/Conexion.php";
require_once "Persistencia/TurnoDAO.php";

  class Turno{

    private $id_turno;
    private $nombre;
    private $conexion;
    private $turnoDAO;

        public function getId_Turno(){
            return $this -> id_turno;
        }

        public function getNombre(){
            return $this -> nombre;
        }

        public function Turno($id_turno="",$nombre=""){
              $this -> id_turno = $id_turno;
              $this -> nombre = $nombre;
              $this -> conexion = new Conexion();
              $this -> turnoDAO = new TurnoDAO($this -> id_turno, $this -> nombre);
        }

  }

?>
