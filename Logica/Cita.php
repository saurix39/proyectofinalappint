<?php
require_once "Persistencia/Conexion.php";
require_once "Persistencia/CitaDAO.php";

  class Cita{

    private $idCita;
    private $id_cliente_fk;
    private $id_doctor_fk;
    private $id_peticion_fk;
    private $id_horario_fk;
    private $fecha;
    private $informacion;
    private $estado;
    private $conexion;
    private $citaDAO;

        public function getIdCita(){
            return $this -> idCita;
        }

        public function getId_cliente_fk(){
            return $this -> id_cliente_fk;
        }

        public function getId_doctor_fk(){
            return $this -> id_doctor_fk;
        }

        public function getId_peticion_fk(){
            return $this -> id_peticion_fk;
        }

        public function getId_horario_fk(){
            return $this -> id_horario_fk;
        }

        public function getFecha(){
            return $this -> fecha;
        }

        public function getInformacion(){
            return $this -> informacion;
        }

        public function getEstado(){
            return $this -> estado;
        }

        public function Cita($idCita="", $id_cliente_fk="", $id_doctor_fk="",$id_peticion_fk="",$id_horario_fk="",$fecha="",$informacion="",$estado=""){
              $this -> idCita = $idCita;
              $this -> id_cliente_fk = $id_cliente_fk;
              $this -> id_doctor_fk = $id_doctor_fk;
              $this -> id_peticion_fk = $id_peticion_fk;
              $this -> id_horario_fk = $id_horario_fk;
              $this -> fecha = $fecha;
              $this -> informacion = $informacion;
              $this -> estado = $estado;
              $this -> conexion = new Conexion();
              $this -> citaDAO = new CitaDAO($this -> idCita, $this -> id_cliente_fk, $this -> id_doctor_fk, $this -> id_peticion_fk, $this -> id_horario_fk, $this -> fecha, $this -> informacion, $this -> estado);
        }

  }

?>
