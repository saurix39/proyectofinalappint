<?php
require_once "Persistencia/Conexion.php";
require_once "Persistencia/PeticionDAO.php";

  class Peticion{

    private $idPeticion;
    private $id_evaluador_fk;
    private $id_cliente_fk;
    private $especialidad;
    private $informacion;
    private $estado;
    private $conexion;
    private $peticionDAO;

        public function getIdPeticion(){
            return $this -> idPeticion;
        }

        public function getId_evaluador_fk(){
            return $this -> id_evaluador_fk;
        }

        public function getId_cliente_fk(){
            return $this -> id_cliente_fk;
        }

        public function getEspecialidad(){
            return $this -> especialidad;
        }

        public function getInformacion(){
            return $this -> informacion;
        }

        public function getEstado(){
            return $this -> estado;
        }

        public function Peticion($idPeticion="",$id_evaluador_fk="",$id_cliente_fk="",$especialidad="",$informacion="",$estado=""){
              $this -> idPeticion = $idPeticion;
              $this -> id_evaluador_fk = $id_evaluador_fk;
              $this -> id_cliente_fk = $id_cliente_fk;
              $this -> especialidad = $especialidad;
              $this -> informacion = $informacion;
              $this -> estado = $estado;
              $this -> conexion = new Conexion();
              $this -> peticionDAO = new PeticionDAO($this -> idPeticion, $this -> id_evaluador_fk, $this -> id_cliente_fk, $this -> especialidad, $this -> informacion, $this -> estado);
        }

  }

?>
