<?php
require_once "Persistencia/Conexion.php";
require_once "Persistencia/HorarioDAO.php";

  class Horario{

    private $idHorario;
    private $id_turno_fk;
    private $hora_inicio;
    private $hora_final;
    private $conexion;
    private $horarioDAO;

        public function getIdHorario(){
            return $this -> idHorario;
        }

        public function getId_turno_fk(){
            return $this -> id_turno_fk;
        }

        public function getHora_inicio(){
            return $this -> hora_inicio;
        }

        public function getHora_final(){
            return $this -> hora_final;
        }

        public function Horario($idHorario="", $id_turno_fk="", $hora_inicio="",$hora_final=""){
              $this -> idHorario = $idHorario;
              $this -> id_turno_fk = $id_turno_fk;
              $this -> hora_inicio = $hora_inicio;
              $this -> hora_final = $hora_final;
              $this -> conexion = new Conexion();
              $this -> horarioDAO = new HorarioDAO($this -> idHorario, $this -> id_turno_fk, $this -> hora_inicio, $this -> hora_final);
        }

  }

?>
