<?php
require_once "Persistencia/Conexion.php";
require_once "Persistencia/ObservacionDAO.php";

  class Observacion{

    private $idObservacion;
    private $id_peticion_fk;
    private $informacion;
    private $conexion;
    private $observacionDAO;

        public function getIdObservacion(){
            return $this -> idObservacion;
        }

        public function getId_peticion_fk(){
            return $this -> id_peticion_fk;
        }

        public function getInformacion(){
            return $this -> informacion;
        }

        public function Observacion($idObservacion="", $id_peticion_fk="", $informacion=""){
              $this -> idObservacion = $idObservacion;
              $this -> id_peticion_fk = $id_peticion_fk;
              $this -> informacion = $informacion;
              $this -> conexion = new Conexion();
              $this -> observacionDAO = new ObservacionDAO($this -> idObservacion,$this -> id_peticion_fk,$this -> informacion);
        }

  }

?>
