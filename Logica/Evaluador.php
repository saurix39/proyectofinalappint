<?php
require_once "Persistencia/Conexion.php";
require_once "Persistencia/EvaluadorDAO.php";

  class Evaluador{

    private $idEvaluador;
    private $cedula;
    private $correo;
    private $clave;
    private $nombre;
    private $apellido;
    private $fech_nac;
    private $estado;
    private $foto;
    private $conexion;
    private $evaluadorDAO;

        public function getIdEvaluador(){
            return $this -> idEvaluador;
        }

        public function getCedula(){
            return $this -> cedula;
        }

        public function getCorreo(){
            return $this -> correo;
        }

        public function getClave(){
            return $this -> clave;
        }

        public function getNombre(){
            return $this -> nombre;
        }

        public function getApellido(){
            return $this -> apellido;
        }

        public function getFech_nac(){
            return $this -> fech_nac;
        }

        public function getEstado(){
            return $this -> estado;
        }

        public function getFoto(){
            return $this -> foto;
        }

        public function Evaluador($idEvaluador="", $cedula="", $correo="",$clave="",$nombre="",$apellido="",$fech_nac="",$estado="",$foto=""){
              $this -> idEvaluador = $idEvaluador;
              $this -> cedula = $cedula;
              $this -> correo = $correo;
              $this -> clave = $clave;
              $this -> nombre = $nombre;
              $this -> apellido = $apellido;
              $this -> fech_nac = $fech_nac;
              $this -> estado = $estado;
              $this -> foto = $foto;
              $this -> conexion = new Conexion();
              $this -> evaluadorDAO = new EvaluadorDAO($this -> idEvaluador, $this -> cedula, $this -> correo, $this -> clave, $this -> nombre,  $this -> apellido, $this -> fech_nac, $this -> estado, $this -> foto);
        }
    public function autenticar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> evaluadorDAO -> autenticar());
        $this -> conexion -> cerrar();
        if($this -> conexion -> numFilas()==1){
            $resultado=$this -> conexion -> extraer();
            $this -> idEvaluador = $resultado[0];
            $this -> estado = $resultado[1];
            return 1;
        }else{
            return 0;
        }
    }

    public function consultar(){
        $this -> conexion ->abrir();
        $this -> conexion ->ejecutar($this -> evaluadorDAO -> consultar());
        $this -> conexion ->cerrar();
        $resultado= $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> apellido = $resultado[1];
        $this -> correo = $resultado[2];
        $this -> foto=$resultado[3];
        $this -> cedula=$resultado[4];
        $this -> fech_nac=$resultado[5];
    }

    public function existeCorreo(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> evaluadorDAO -> existeCorreo());
        $this -> conexion -> cerrar();
        if($this -> conexion -> numFilas()>=1){
            return 1;
        }else{
            return 0;
        }
    }

    public function registrarEvaluador(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> evaluadorDAO -> registrarEvaluador());
        $this -> conexion -> cerrar();
    }

    public function actualizarInformacion(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> evaluadorDAO -> actualizarInformacion());
        $this -> conexion -> cerrar();
    }

        public function consultarClientes(){
          $this -> conexion -> abrir();
          $this -> conexion -> ejecutar($this -> evaluadorDAO -> consultarEvaluadores());
          $evaarray = array();
          while(($resultado = $this -> conexion -> extraer()) != null){
            $eva = new Evaluador($resultado[0], $resultado[1], $resultado[2],"", $resultado[3],$resultado[4], $resultado[5], $resultado[6], $resultado[7]);
            array_push($evaarray,$eva);
          }
          $this -> conexion -> cerrar();
          return $evaarray;
        }

        public function consultarFiltroPaginacion($filtro,$cantidad,$pagina){
          $this -> conexion -> abrir();
          $this -> conexion -> ejecutar($this -> evaluadorDAO -> consultarFiltroPaginacion($filtro,$cantidad,$pagina));
          $evaarray = array();
          while(($resultado = $this -> conexion -> extraer()) != null){
            $eva = new Evaluador($resultado[0], $resultado[1], $resultado[2],"", $resultado[3],$resultado[4], $resultado[5], $resultado[6], $resultado[7]);
            array_push($evaarray,$eva);
          }
          $this -> conexion -> cerrar();
          return $evaarray;
        }


        public function consultarCantidad($filtro){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> evaluadorDAO -> consultarCantidad($filtro));
            return $this -> conexion -> extraer();
        }

        public function actualizar_Estado_Eva(){
          $this -> conexion -> abrir();
          $this -> conexion -> ejecutar($this -> evaluadorDAO -> actualizar_Estado_Eva());
          $this -> conexion -> cerrar();
      }

  }

?>
