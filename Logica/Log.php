<?php
  require_once "Persistencia/Conexion.php";
  require_once "Persistencia/LogDAO.php";

  class Log{

    private $idLog;
    private $idAccion;
    private $idActor;
    private $idTipo;
    private $informacion;
    private $fecha;
    private $hora;
    private $conexion;
    private $logDAO;

        public function getIdLog(){
            return $this -> idLog;
        }

        public function getIdAccion(){
            return $this -> idAccion;
        }

        public function getIdActor(){
            return $this -> idActor;
        }

        public function getIdTipo(){
            return $this -> idTipo;
        }

        public function getInformacion(){
            return $this -> informacion;
        }

        public function getFecha(){
            return $this ->  fecha;
        }

        public function getHora(){
            return $this -> hora;
        }

        public function Log($idLog="",$idAccion="",$idActor="",$idTipo="",$informacion="",$fecha="",$hora=""){
            $this -> idLog = $idLog;
            $this -> idAccion = $idAccion;
            $this -> idActor = $idActor;
            $this -> idTipo = $idTipo;
            $this -> informacion = $informacion;
            $this -> fecha = $fecha;
            $this -> hora = $hora;
            $this -> conexion = new Conexion();
            $this -> logDAO = new LogDAO($this -> idLog,$this -> idAccion,$this -> idActor,$this -> idTipo,$this -> informacion,$this -> fecha,$this -> hora);
        }
}
?>
