<?php
$correo=$_POST["correo"];
$clave=$_POST["clave"];
$administrador=new Administrador("","",$correo,$clave);
$cliente=new Cliente("","",$correo,$clave);
$doctor=new Doctor("","",$correo,$clave);
$evaluador=new Evaluador("","",$correo,$clave);
date_default_timezone_set("America/Bogota");
if($administrador -> autenticar()){
    if($administrador -> getEstado()==0){
        header("Location: index.php?error=3");
    }else{
        $_SESSION["id"]=$administrador -> getIdAdministrador();
        $_SESSION["rol"]="Administrador";
        //$informacion="Ingreso el siguiente administrador:<br>identificación: ". $administrador -> getIdAdministrador(). "<br>correo: ".$correo;
        //$fecha=date('Y-m-j G-i-s');
        //$log = new Log("","2",$_SESSION["id"],"",$informacion,$fecha);
        //$log -> insertarLogAdministrador();
        header("Location: index.php?pid=".base64_encode("Presentacion/Administrador/sesionAdministrador.php"));
    }
}else if($cliente -> autenticar()){
    if($cliente -> getEstado()==-1){
        header("Location: index.php?error=2");
    }else if($cliente -> getEstado()==0){
        header("Location: index.php?error=3");
    }else{
        $_SESSION["id"]=$cliente -> getIdCliente();
        $_SESSION["rol"]="Cliente";
        //$informacion="Ingreso el siguiente cliente:<br>identificación: ". $cliente -> getIdCliente(). "<br>correo: ".$correo;
        //$fecha=date('Y-m-j G-i-s');
        //$log = new Log("","2",$_SESSION["id"],"",$informacion,$fecha);
        //$log -> insertarLogCliente();
        header("Location: index.php?pid=".base64_encode("Presentacion/Cliente/sesionCliente.php"));
    }
}else if($doctor -> autenticar()){
    if($doctor -> getEstado()==0){
        header("Location: index.php?error=3");
    }else{
        $_SESSION["id"]=$doctor -> getIdDoctor();
        $_SESSION["rol"]="Doctor";
        //$informacion="Ingreso el siguiente proveedor:<br>identificación: ". $proveedor -> getIdProveedor(). "<br>correo: ".$correo;
        //$fecha=date('Y-m-j G-i-s');
        //$log = new Log("","2",$_SESSION["id"],"",$informacion,$fecha);
        //$log -> insertarLogProveedor();
        header("Location: index.php?pid=".base64_encode("Presentacion/Doctor/sesionDoctor.php"));
    }
}else if($evaluador -> autenticar()){
    if($evaluador -> getEstado()==0){
        header("Location: index.php?error=3");
    }else{
        $_SESSION["id"]=$evaluador -> getIdEvaluador();
        $_SESSION["rol"]="Evaluador";
        //$informacion="Ingreso el siguiente proveedor:<br>identificación: ". $proveedor -> getIdProveedor(). "<br>correo: ".$correo;
        //$fecha=date('Y-m-j G-i-s');
        //$log = new Log("","2",$_SESSION["id"],"",$informacion,$fecha);
        //$log -> insertarLogProveedor();
        header("Location: index.php?pid=".base64_encode("Presentacion/Evaluador/sesionEvaluador.php"));
    }
}else{
    header("Location: index.php?pid='". base64_encode("Presentacion/formAutenticar.php") ."'&error=1");
}
?>
