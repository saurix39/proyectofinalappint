<?php
$administrador= new Administrador($_SESSION["id"]);
$administrador -> consultar();
?>
<div class="container">
    <div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-8 col-12">
            <div class="card border border-secondary mt-1">  
                <div class="d-flex justify-content-center">
                        <img src="<?php echo ($administrador -> getFoto()=="")?"https://icons.iconarchive.com/icons/custom-icon-design/pretty-office-2/256/man-icon.png":$administrador -> getFoto() ?>" alt="foto del usuario" width="30%">
                </div>
                <table class="table mt-1">
                    <tbody>
                        <tr>
                        <th scope="row">Nombre</th>
                        <td><?php echo $administrador ->getNombre() ?></td>
                        </tr>
                        <tr>
                        <th scope="row">Apellido</th>
                        <td><?php echo $administrador ->getApellido() ?></td>
                        </tr>
                        <tr>
                        <th scope="row">Cedula</th>
                        <td><?php echo $administrador ->getCedula() ?></td>
                        </tr>
                        <tr>
                        <th scope="row">Correo</th>
                        <td><?php echo $administrador ->getCorreo() ?></td>
                        </tr>
                        <tr>
                        <th scope="row">Fecha de nacimiento</th>
                        <td><?php echo $administrador ->getFech_nac() ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>