<?php
$eva = new Evaluador();
$cantidad = 15;
if (isset($_GET["cantidad"])) {
    $cantidad = $_GET["cantidad"];
}
$pagina = 1;
if (isset($_GET["pagina"])) {
    $pagina = $_GET["pagina"];
}
$filtro = "";
if (isset($_GET["filtro"])) {
    $filtro = $_GET["filtro"];
}
$cant = $eva -> consultarCantidad($filtro);
$cantPagina = intval($cant[0] / $cantidad);
if (($cant[0] % $cantidad) != 0) {
    $cantPagina++;
}
$evaluadores = $eva -> consultarFiltroPaginacion($filtro, $cantidad, $pagina);
?>
<div class="container mt-3">
    <div class="row">
        <div class="col-12 col-lg-3">
          <div class="card">
              <div class="card-header">
                  <h3 style="font-family: 'Playfair Display', serif; font-size:30px">Filtro</h3>
              </div>
              <div class="card-body">
                <input class="form-control mr-sm-2" id="buscar" type="search" placeholder="Buscar por cedula" aria-label="Search" data-cantidad="<?php echo $cantidad ?>" value="<?php echo ($filtro != null ? $filtro : "") ?>">
              </div>
          </div>
        </div>
        <div class="col-12 col-lg-9">
          <div class="card">
              <div class="card-header text-white bg-primary">
                  <h4 style="font-family: 'Playfair Display', serif; font-size:30px">Lista de evaluadores</h4>
              </div>
              <div class="card-body">
                  <div id="contenido">
                      <div class="table table-responsive-sm table-responsive-md">
                        <table class="table table-responsive-md table-hover table-striped">
                          <tr class="thead-dark">
                            <th>#</th>
                            <th>Cedula</th>
                            <th>Nombre</th>
                            <th>Apellido</th>
                            <th>Fecha Nac.</th>
                            <th>Estado</th>
                            <th></th>
                          </tr>
                          <?php
                          $i=1;
                          foreach($evaluadores as $evas){
                              echo "<tr>";
                              echo "<td>" . $i . "</td>";
                              echo "<td>" . $evas -> getCedula() . "</td>";
                              echo "<td>" . $evas -> getNombre() . "</td>";
                              echo "<td>" . $evas -> getApellido() . "</td>";
                              echo "<td>" . $evas -> getFech_nac() . "</td>";
                          ?>
                            <td>
                              <select class="custom-select camestado_eva" data-ideva="<?php echo $evas -> getIdEvaluador()?>" style="width: 100px;" >
                                <option value="1" <?php echo ($evas->getEstado() == 1) ? 'selected' : '' ?>>Activo</option>
                                <option value="0" <?php echo ($evas->getEstado() == 0) ? 'selected' : '' ?>>Inactivo</option>
                              </select>
                            </td>
                          <?php
                              echo "</tr>";
                              $i++;
                          }
                          ?>
                        </table>
                      </div>
                      <div class="d-flex justify-content-end">
                          <nav>
                              <ul class="pagination">
                                  <?php if ($pagina > 1) {
                                      echo '<li class="page-item"> <a class="page-link" href="index.php?pid=' . base64_encode("Presentacion/Administrador/Evaluador/listaEvaluador.php") . '&pagina=' . ($pagina - 1) . '&cantidad=' . $cantidad . '&filtro=' . $filtro . '" tabindex="0" aria-disabled="false"> &lt;&lt; </a></li>';
                                  } ?>
                                  <?php for ($i = 1; $i <= $cantPagina; $i++) {
                                      if ($pagina == $i) {
                                          echo "<li class='page-item active'>" .
                                              "<a class='page-link'>$i</a>" .
                                              "</li>";
                                      } else {
                                          echo "<li class='page-item'>" .
                                              "<a class='page-link' href='index.php?pid=" . base64_encode("Presentacion/Administrador/Evaluador/listaEvaluador.php") . "&pagina=" . $i . "&cantidad=" . $cantidad . "&filtro=" . $filtro . "'>" . $i . "</a>" .
                                              "</li>";
                                      }
                                  } ?>
                                  <?php if ($pagina < $cantPagina) {
                                      echo '<li class="page-item"> <a class="page-link" href="index.php?pid=' . base64_encode("Presentacion/Administrador/Evaluador/listaEvaluador.php") . '&pagina=' . ($pagina + 1) . '&cantidad=' . $cantidad . '&filtro=' . $filtro . '" tabindex="0" aria-disabled="false"> &gt;&gt; </a></li>';
                                  } ?>
                              </ul>
                          </nav>
                      </div>
                  </div>
              </div>
          </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $("#buscar").keyup(function() {
            if ($(this).val().length >= 1 || $(this).val().length == 0) {
                var url = "indexAjax.php?pid=<?php echo base64_encode("Presentacion/Administrador/Evaluador/listaEvaluadorAjax.php") ?>&filtro=" + $(this).val() + "&cantidad=" + $(this).data("cantidad");
                $("#contenido").load(url);
            }
        });
    });
</script>

<script>
  $(".camestado_eva").change(function() {
      var objJSON = {
          idEva: $(this).data("ideva"),
          estado: $(this).val(),
      }

      url = "indexAjax.php?pid=<?php echo base64_encode("Presentacion/Administrador/Evaluador/cambioEstadoEvaAjax.php") ?>";

      $.post(url, objJSON, function(info) {
          $res = JSON.parse(info);
      })

  });
</script>
