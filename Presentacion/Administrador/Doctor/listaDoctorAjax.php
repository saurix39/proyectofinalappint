<?php
$doc = new Doctor();
$cantidad = 15;
if (isset($_GET["cantidad"])) {
    $cantidad = $_GET["cantidad"];
}
$pagina = 1;
if (isset($_GET["pagina"])) {
    $pagina = $_GET["pagina"];
}
$filtro = "";
if (isset($_GET["filtro"])) {
    $filtro = $_GET["filtro"];
}
$cant = $doc -> consultarCantidad($filtro);
$cantPagina = intval($cant[0] / $cantidad);
if (($cant[0] % $cantidad) != 0) {
    $cantPagina++;
}
$doctores = $doc -> consultarFiltroPaginacion($filtro, $cantidad, $pagina);
?>
<div id="contenido">
    <div class="table table-responsive-sm table-responsive-md">
      <table class="table table-responsive-md table-hover table-striped">
        <tr class="thead-dark">
          <th>#</th>
          <th>Cedula</th>
          <th>Especialidad</th>
          <th>Nombre</th>
          <th>Apellido</th>
          <th>Fecha Nac.</th>
          <th>Estado</th>
          <th></th>
        </tr>
        <?php
        $i=1;
        foreach($doctores as $docs){
            echo "<tr>";
            echo "<td>" . $i . "</td>";
            echo "<td>" . $docs -> getCedula() . "</td>";
            echo "<td>" . $docs -> getEspecialidad() . "</td>";
            echo "<td>" . $docs -> getNombre() . "</td>";
            echo "<td>" . $docs -> getApellido() . "</td>";
            echo "<td>" . $docs -> getFech_nac() . "</td>";
        ?>
          <td>
            <select class="custom-select camestado_doc" data-iddoc="<?php echo $docs -> getIdDoctor()?>" style="width: 100px;" >
              <option value="1" <?php echo ($docs->getEstado() == 1) ? 'selected' : '' ?>>Activo</option>
              <option value="0" <?php echo ($docs->getEstado() == 0) ? 'selected' : '' ?>>Inactivo</option>
            </select>
          </td>
        <?php
            echo "</tr>";
            $i++;
        }
        ?>
      </table>
    </div>
    <div class="d-flex justify-content-end">
        <nav>
            <ul class="pagination">
                <?php if ($pagina > 1) {
                    echo '<li class="page-item"> <a class="page-link" href="index.php?pid=' . base64_encode("Presentacion/Administrador/Doctor/listaDoctor.php") . '&pagina=' . ($pagina - 1) . '&cantidad=' . $cantidad . '&filtro=' . $filtro . '" tabindex="0" aria-disabled="false"> &lt;&lt; </a></li>';
                } ?>
                <?php for ($i = 1; $i <= $cantPagina; $i++) {
                    if ($pagina == $i) {
                        echo "<li class='page-item active'>" .
                            "<a class='page-link'>$i</a>" .
                            "</li>";
                    } else {
                        echo "<li class='page-item'>" .
                            "<a class='page-link' href='index.php?pid=" . base64_encode("Presentacion/Administrador/Doctor/listaDoctor.php") . "&pagina=" . $i . "&cantidad=" . $cantidad . "&filtro=" . $filtro . "'>" . $i . "</a>" .
                            "</li>";
                    }
                } ?>
                <?php if ($pagina < $cantPagina) {
                    echo '<li class="page-item"> <a class="page-link" href="index.php?pid=' . base64_encode("Presentacion/Administrador/Doctor/listaDoctor.php") . '&pagina=' . ($pagina + 1) . '&cantidad=' . $cantidad . '&filtro=' . $filtro . '" tabindex="0" aria-disabled="false"> &gt;&gt; </a></li>';
                } ?>
            </ul>
        </nav>
    </div>
</div>

<script>
  $(".camestado_doc").change(function() {
      var objJSON = {
          idDoc: $(this).data("iddoc"),
          estado: $(this).val(),
      }

      url = "indexAjax.php?pid=<?php echo base64_encode("Presentacion/Administrador/Doctor/cambioEstadoDocAjax.php") ?>";

      $.post(url, objJSON, function(info) {
          $res = JSON.parse(info);
      })

  });
</script>
