<?php
$actualizado=false;
$doctor= new Doctor($_SESSION["id"]);
$doctor -> consultar();
$nombres =  $doctor -> obtenerNombres();
if(isset($_POST["actualizar"])){
    $idnombre=$doctor -> consultarIdNombre($_POST["especialidad"]);
    //date_default_timezone_set("America/Bogota");
    $cedula=$_POST["cedula"];
    $nombre=$_POST["nombre"];
    $apellido=$_POST["apellido"];
    $fech_nac=$_POST["fech_nac"];
    if($_FILES["imagen"]["name"] != ""){
        $rutaLocal = $_FILES["imagen"]["tmp_name"];
        $tipo = $_FILES["imagen"]["type"];
        $tiempo = new DateTime();
        $rutaRemota = "Imagenes/Doctor/" . $tiempo -> getTimestamp() . (($tipo == "image/png")?".png":".jpg");
        copy($rutaLocal,$rutaRemota);
        if($doctor -> getFoto()!=""){
            unlink($doctor -> getFoto());
        }
        $doctor = new Doctor($_SESSION["id"],$cedula,"","",$idnombre,$nombre,$apellido,$fech_nac,"1",$rutaRemota, $_POST["turno"]);
        $doctor -> actualizarInformacion();
        //$administrador -> autenticar();
        //$informacion="Se registro un Proveedor con la siguiente información:<br>identificación: ". $proveedor -> getIdProveedor() ."<br>nombre: ". $nombre ."<br>correo: ". $correo ."<br>nit: ". $nit;
        //$fecha=date('Y-m-j G-i-s');
        //$log = new Log("","1",$_SESSION["id"],"",$informacion,$fecha);
        //$log -> insertarLogAdministrador();
        $actualizado=true;
    }
}
?>
<div class="container">
    <div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-8 col-12">
            <div class="card border border-secondary mt-1">
                <div class="card-header text-center">
                    <h2>Actualizar información</h2>
                </div>
                <div class="card-body">
                    <form action="index.php?pid=<?php echo base64_encode("Presentacion/Doctor/actuInfoDoctor.php")?>" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label>Cedula</label>
                            <input type="text" name="cedula" class="form-control" value="<?php echo $doctor-> getCedula() ?>" required>
                        </div>
                        <div class="form-group">
                            <label>Nombre</label>
                            <input type="text" name="nombre" class="form-control" value="<?php echo $doctor-> getNombre() ?>" required>
                        </div>
                        <div class="form-group">
                            <label>Apellido</label>
                            <input type="text" name="apellido" class="form-control" value="<?php echo $doctor-> getApellido() ?>" required>
                        </div>
                        <div class="form-group">
                            <label>Fecha de nacimiento</label>
                            <input type="date" name="fech_nac" class="form-control" value="<?php echo $doctor-> getFech_nac() ?>" required>
                        </div>
                        <label>Especialidad</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="inputGroupSelect01">Opciones</label>
                            </div>
                            <select class="custom-select" name="especialidad" required>
                                <option selected>Escoger...</option>
                                <?php
                                foreach($nombres as $nombreactual){
                                    echo "<option value='". $nombreactual ."'>". $nombreactual ."</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <label>Turno</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="inputGroupSelect01">Opciones</label>
                            </div>
                            <select class="custom-select" name="turno" required>
                                <option selected>Escoger...</option>
                                <option value='1'>Mañana</option>
                                <option value='2'>Tarde</option>
                            </select>
                        </div>
                        <label>Imagen</label>
                        <div class="custom-file mb-3">
                            <input type="file" name="imagen" class="custom-file-input" id="validatedCustomFile" required>
                            <label class="custom-file-label" for="validatedCustomFile">Escoja la imagen...</label>
                        </div>
                        <div class="form-group">
                            <button type="submit" name="actualizar" class="form-control btn btn-secondary">Actualizar informacion</button>
                        </div> 
                        <?php if($actualizado){?>
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                Datos ingresados con exito!!
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <?php } ?>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>