<?php
$cliente= new Cliente($_SESSION["id"]);
$cliente -> consultar();
?>
<div class="container">
    <div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-8 col-12">
            <div class="card border border-secondary mt-1">  
                <div class="d-flex justify-content-center">
                        <img src="<?php echo ($cliente-> getFoto()=="")?"https://icons.iconarchive.com/icons/custom-icon-design/pretty-office-2/256/man-icon.png":$cliente -> getFoto() ?>" alt="foto del usuario" width="30%">
                </div>
                <table class="table mt-1">
                    <tbody>
                        <tr>
                        <th scope="row">Nombre</th>
                        <td><?php echo $cliente ->getNombre() ?></td>
                        </tr>
                        <tr>
                        <th scope="row">Apellido</th>
                        <td><?php echo $cliente ->getApellido() ?></td>
                        </tr>
                        <tr>
                        <th scope="row">Cedula</th>
                        <td><?php echo $cliente ->getCedula() ?></td>
                        </tr>
                        <tr>
                        <th scope="row">Correo</th>
                        <td><?php echo $cliente ->getCorreo() ?></td>
                        </tr>
                        <tr>
                        <th scope="row">Fecha de nacimiento</th>
                        <td><?php echo $cliente ->getFech_nac() ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>