<?php
$actualizado=false;
if(isset($_POST["actualizar"])){
    //date_default_timezone_set("America/Bogota");
    $cedula=$_POST["cedula"];
    $nombre=$_POST["nombre"];
    $apellido=$_POST["apellido"];
    $fech_nac=$_POST["fech_nac"];
    $evaluador =new Evaluador($_SESSION["id"]);
    $evaluador -> consultar();
    if($_FILES["imagen"]["name"] != ""){
        $rutaLocal = $_FILES["imagen"]["tmp_name"];
        $tipo = $_FILES["imagen"]["type"];
        $tiempo = new DateTime();
        $rutaRemota = "Imagenes/Evaluador/" . $tiempo -> getTimestamp() . (($tipo == "image/png")?".png":".jpg");
        copy($rutaLocal,$rutaRemota);
        if($evaluador -> getFoto()!=""){
            unlink($evaluador -> getFoto());
        }
        $evaluador = new Evaluador($_SESSION["id"],$cedula,"","",$nombre,$apellido,$fech_nac,"1",$rutaRemota);
        $evaluador -> actualizarInformacion();
        //$administrador -> autenticar();
        //$informacion="Se registro un Proveedor con la siguiente información:<br>identificación: ". $proveedor -> getIdProveedor() ."<br>nombre: ". $nombre ."<br>correo: ". $correo ."<br>nit: ". $nit;
        //$fecha=date('Y-m-j G-i-s');
        //$log = new Log("","1",$_SESSION["id"],"",$informacion,$fecha);
        //$log -> insertarLogAdministrador();
        $actualizado=true;
    }
}
?>
<div class="container">
    <div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-8 col-12">
            <div class="card border border-secondary mt-1">
                <div class="card-header text-center">
                    <h2>Actualizar información</h2>
                </div>
                <div class="card-body">
                    <form action="index.php?pid=<?php echo base64_encode("Presentacion/Evaluador/actuInfoEvaluador.php")?>" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label>Cedula</label>
                            <input type="text" name="cedula" class="form-control" value="<?php echo $evaluador-> getCedula() ?>" required>
                        </div>
                        <div class="form-group">
                            <label>Nombre</label>
                            <input type="text" name="nombre" class="form-control" value="<?php echo $evaluador-> getNombre() ?>" required>
                        </div>
                        <div class="form-group">
                            <label>Apellido</label>
                            <input type="text" name="apellido" class="form-control" value="<?php echo $evaluador-> getApellido() ?>" required>
                        </div>
                        <div class="form-group">
                            <label>Fecha de nacimiento</label>
                            <input type="date" name="fech_nac" class="form-control" value="<?php echo $evaluador-> getFech_nac() ?>" required>
                        </div>
                        <label>Imagen</label>
                        <div class="custom-file mb-3">
                            <input type="file" name="imagen" class="custom-file-input" id="validatedCustomFile" required>
                            <label class="custom-file-label" for="validatedCustomFile">Escoja la imagen...</label>
                        </div>
                        <div class="form-group">
                            <button type="submit" name="actualizar" class="form-control btn btn-secondary">Actualizar información</button>
                        </div> 
                        <?php if($actualizado){?>
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                Datos ingresados con exito!!
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <?php } ?>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>