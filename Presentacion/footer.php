<div class="container">
    <div class="row">
        <div class="col">
            <div class="card-body bg-primary mt-2 text-center text-white mb-1">
                <div class="row">
                    <div class="col">
                        <h2 class="card-title">Quick Medicine entidad de servicios de salud</h2>
                        <p class="card-text">©Quick Medicine 2020
                        <br>
                        Todos los derechos reservados
                        <br>
                        NIT 99958.22589.322
                        <br>
                        Para reportar errores comuniquese a: QuickMedicineErrores@gmail.com
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>