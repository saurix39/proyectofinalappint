<?php
require_once "Logica/Administrador.php";
require_once "Logica/Cliente.php";
require_once "Logica/Doctor.php";
require_once "Logica/Evaluador.php";
require_once "Logica/Cita.php";

session_start();
$error=0;
if(isset($_GET["error"])){
$error=$_GET["error"];
}
$pos="";
if(isset($_GET["pos"])){
    $pos=$_GET["pos"];
}
$pid="";
if(isset($_GET["pid"])){
    $pid=base64_decode($_GET["pid"]);
}else{
    $_SESSION["id"]="";
    $_SESSION["rol"]="";
}
if(isset($_GET["cerrarSesion"])||!isset($_SESSION["id"])){
    $_SESSION["id"]="";
    $_SESSION["rol"]="";
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="icon" href="Img/logo-pequeño.jpg">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.1/css/all.css"/>
    <script>
        $(function () {
	    $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
    <title>Quick Medicine</title>
</head>
<body>
    <?php
        $paginasSinSesion= array(
            "Presentacion/Autenticar.php"
        );
        $paginasDeInicio= array(
            //"Presentacion/Servicios.php",
            //"Presentacion/sobreNosotros.php"
        );

        if(in_array($pid, $paginasSinSesion)){
            include $pid;
        }else if($_SESSION["id"]!=""){
            if($_SESSION["rol"]=="Administrador"){
                include "Presentacion/Administrador/menuAdministrador.php";
            }else if($_SESSION["rol"]=="Cliente"){
                include "Presentacion/Cliente/menuCliente.php";
            }else if($_SESSION["rol"]=="Doctor"){
                include "Presentacion/Doctor/menuDoctor.php";
            }else if($_SESSION["rol"]=="Evaluador"){
                include "Presentacion/Evaluador/menuEvaluador.php";
            }
            include $pid;
            include "Presentacion/footer.php";
        }else{
            include "Presentacion/encabezado.php";
            include "Presentacion/menu.php";
            if(in_array($pid,$paginasDeInicio)){
                include $pid;
            }else{
                include "Presentacion/inicio.php";
            }
            include "Presentacion/footer.php";
        }
    ?>
</body>
</html>

<?php
if(isset($_GET["error"])){
    if($error==3 || $error==1){
        ?>
        <script>
            $("#Autenticar").modal();
        </script>
        <?php
    }
}
?>
