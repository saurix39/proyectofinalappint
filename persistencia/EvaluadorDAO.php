<?php
  class EvaluadorDAO{

    private $idEvaluador;
    private $cedula;
    private $correo;
    private $clave;
    private $nombre;
    private $apellido;
    private $fech_nac;
    private $estado;
    private $foto;

        public function EvaluadorDAO($idEvaluador="", $cedula="", $correo="",$clave="",$nombre="",$apellido="",$fech_nac="",$estado="",$foto=""){
              $this -> idEvaluador = $idEvaluador;
              $this -> cedula = $cedula;
              $this -> correo = $correo;
              $this -> clave = $clave;
              $this -> nombre = $nombre;
              $this -> apellido = $apellido;
              $this -> fech_nac = $fech_nac;
              $this -> estado = $estado;
              $this -> foto = $foto;
       }
    public function autenticar(){
      return "select id_evaluador, estado from evaluador where correo='". $this -> correo ."' and clave='". md5($this -> clave) ."'";
    }

    public function consultar(){
      return "select nombre, apellido, correo, foto, cedula, fech_nac from evaluador where id_evaluador='". $this -> idEvaluador ."'";
    }

    public function existeCorreo(){
      return "select correo from evaluador where correo='". $this -> correo ."'";
    }

    public function registrarEvaluador(){
      return "insert into evaluador (cedula,nombre,apellido,correo,clave,foto,fech_nac,estado) values ('". $this -> cedula ."','". $this -> nombre ."','". $this -> apellido ."','". $this -> correo ."','". md5($this -> clave) ."','". (($this -> foto != "")?$this -> foto:"") ."','". $this -> fech_nac ."','1')";
    }

    public function actualizarInformacion(){
      return "update evaluador set cedula='". $this -> cedula ."', nombre='". $this -> nombre ."', apellido='". $this -> apellido ."', fech_nac='". $this -> fech_nac ."', foto='". $this -> foto ."' where id_evaluador='". $this -> idEvaluador ."'";
    }

       public function consultarEvaluadores(){
         return "select id_evaluador,cedula,correo, nombre, apellido, fech_nac, estado,foto
                 from evaluador";
       }

       public function consultarFiltroPaginacion($filtro,$cantidad, $pagina){
             return "select id_evaluador,cedula,correo, nombre, apellido, fech_nac, estado,foto
                     from evaluador
                     where cedula like '%" . $filtro . "%' limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
       }

       public function consultarCantidad($filtro){
         return "select count(id_evaluador)
                 from evaluador
                 where cedula like '%".$filtro."%'";
       }

       public function actualizar_Estado_Eva(){
           return "update evaluador
                   set estado = '" . $this -> estado. "'
                   where id_evaluador = '" . $this -> idEvaluador ."'";
       }

  }

?>
