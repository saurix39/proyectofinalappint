<?php
class AdministradorDAO{
    private $idAdministrador;
    private $cedula;
    private $correo;
    private $clave;
    private $nombre;
    private $apellido;
    private $fech_nac;
    private $estado;
    private $foto;

    public function AdministradorDAO($idAdministrador="", $cedula="", $correo="",$clave="",$nombre="",$apellido="",$fech_nac="",$estado="",$foto=""){
            $this -> idAdministrador = $idAdministrador;
            $this -> cedula = $cedula;
            $this -> correo = $correo;
            $this -> clave = $clave;
            $this -> nombre = $nombre;
            $this -> apellido = $apellido;
            $this -> fech_nac = $fech_nac;
            $this -> estado = $estado;
            $this -> foto = $foto;
    }

    public function autenticar(){
        return "select id_administrador, estado from administrador where correo='". $this -> correo ."' and clave='". md5($this -> clave) ."'";
    }

    public function consultar(){
        return "select nombre, apellido, correo, foto, cedula, fech_nac from administrador where id_administrador='". $this -> idAdministrador ."'";
    }

    public function existeCorreo(){
        return "select correo from administrador where correo='". $this -> correo ."'";
    }

    public function registrarAdministrador(){
        return "insert into administrador (cedula,nombre,apellido,correo,clave,foto,fech_nac,estado) values ('". $this -> cedula ."','". $this -> nombre ."','". $this -> apellido ."','". $this -> correo ."','". md5($this -> clave) ."','". (($this -> foto != "")?$this -> foto:"") ."','". $this -> fech_nac ."','1')";
    }

    public function actualizarInformacion(){
        return "update administrador set cedula='". $this -> cedula ."', nombre='". $this -> nombre ."', apellido='". $this -> apellido ."', fech_nac='". $this -> fech_nac ."', foto='". $this -> foto ."' where id_administrador='". $this -> idAdministrador ."'";
    }


    public function consultarFiltroPaginacion($filtro,$cantidad, $pagina){
          return "select id_administrador,cedula,correo, nombre, apellido, fech_nac, estado,foto
                  from administrador
                  where cedula like '%" . $filtro . "%' limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }

    public function consultarCantidad($filtro){
      return "select count(id_administrador)
              from administrador
              where cedula like '%".$filtro."%'";
    }

    public function actualizar_Estado_Adm(){
        return "update administrador
                set estado = '" . $this -> estado. "'
                where id_administrador = '" . $this -> idAdministrador ."'";
    }

}
?>
