<?php
  class ClienteDAO{

    private $idCliente;
    private $cedula;
    private $correo;
    private $clave;
    private $nombre;
    private $apellido;
    private $fech_nac;
    private $estado;
    private $foto;

        public function ClienteDAO($idCliente="", $cedula="", $correo="",$clave="",$nombre="",$apellido="",$fech_nac="",$estado="",$foto=""){
              $this -> idCliente = $idCliente;
              $this -> cedula = $cedula;
              $this -> correo = $correo;
              $this -> clave = $clave;
              $this -> nombre = $nombre;
              $this -> apellido = $apellido;
              $this -> fech_nac = $fech_nac;
              $this -> estado = $estado;
              $this -> foto = $foto;
        }

      public function autenticar(){
        return "select id_cliente, estado from cliente where correo='". $this -> correo ."' and clave='". md5($this -> clave) ."'";
      }

      public function consultar(){
        return "select nombre, apellido, correo, foto, cedula, fech_nac from cliente where id_cliente='". $this -> idCliente ."'";
      }

      public function existeCorreo(){
        return "select correo from cliente where correo='". $this -> correo ."'";
      }

      public function registrarCliente(){
        return "insert into cliente (cedula,nombre,apellido,correo,clave,foto,fech_nac,estado) values ('". $this -> cedula ."','". $this -> nombre ."','". $this -> apellido ."','". $this -> correo ."','". md5($this -> clave) ."','". (($this -> foto != "")?$this -> foto:"") ."','". $this -> fech_nac ."','1')";
      }

      public function actualizarInformacion(){
        return "update cliente set cedula='". $this -> cedula ."', nombre='". $this -> nombre ."', apellido='". $this -> apellido ."', fech_nac='". $this -> fech_nac ."', foto='". $this -> foto ."' where id_cliente='". $this -> idCliente ."'";
      }
  
        public function consultarClientes(){
          return "select id_cliente,cedula,correo, nombre, apellido, fech_nac, estado,foto
                  from cliente";
        }

        public function consultarFiltroPaginacion($filtro,$cantidad, $pagina){
              return "select id_cliente,cedula,correo, nombre, apellido, fech_nac, estado,foto
                      from cliente
                      where cedula like '%" . $filtro . "%' limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
        }

        public function consultarCantidad($filtro){
          return "select count(id_cliente)
                  from cliente
                  where cedula like '%".$filtro."%'";
        }

        public function actualizar_Estado_Cli(){
            return "update cliente
                    set estado = '" . $this -> estado. "'
                    where id_cliente = '" . $this -> idCliente ."'";
        }

  }

?>
